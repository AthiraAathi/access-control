package services

import (
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

var jwtKey = []byte("secret")

type JwtValidateResponse struct {
	Valid    bool
	ErrorMsg string
	UserId   int
	UuId     string
	Check    bool
}
type Claims struct {
	UserId int    `json:"userid"`
	UuId   string `json:"uuid"`
	Check  bool   `json:"check"`
	jwt.RegisteredClaims
}

// generated a new JWT token from the userid with given expiry time
func GenerateJwtToken(userId int, expTime int, uuId string) (string, error) {

	expirationTime := time.Now().Add(time.Duration(expTime) * time.Minute)
	newClaims := &Claims{
		UserId: userId,
		UuId:   uuId,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expirationTime),
		},
	}
	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, newClaims)
	token, err := jwtToken.SignedString(jwtKey)
	if err != nil {

		return "", err
	}
	return token, err
}

// validates a jwt token for signature and expiry
func ValidateJwtToken(token string) (response JwtValidateResponse) {

	defer func() {
		if rec := recover(); rec != nil {
			fmt.Print("panicked")
			response.ErrorMsg = "panicked"
		}
	}()

	response = JwtValidateResponse{
		Valid:    false,
		UserId:   0,
		ErrorMsg: "",
		UuId:     "",
	}

	claims := &Claims{}
	if len(token) == 0 {
		response.ErrorMsg = "no token"
	} else {
		tokenParsed, err := jwt.ParseWithClaims(token, claims, func(t *jwt.Token) (interface{}, error) {
			return jwtKey, nil
		})
		if claims, ok := tokenParsed.Claims.(*Claims); ok && tokenParsed.Valid {
			response.Valid = true
			response.UserId = claims.UserId
			response.UuId = claims.UuId
		} else {
			response.ErrorMsg = err.Error()
		}
	}
	return response
}
