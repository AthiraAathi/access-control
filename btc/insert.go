package main

import (
	"btc/models"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (env Env) Insert(ctx *gin.Context) {
	var details models.InputDetails

	if err := ctx.BindJSON(&details); err != nil {
		log.Printf("invalid JSON body: %v", err)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "invalid JSON body",
		})
		return
	}
	query := ` INSERT INTO roles (role_name,description) VALUES ( $1 , $2 ) `
	result, err := env.DB.Exec(query, details.Name, details.Description)
	if err != nil {
		log.Printf("error occurred while inserting new record into roles table: %v", err)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "error while inserting",
		})
		return
	}

	// checking the number of rows affected
	n, err := result.RowsAffected()
	if err != nil {
		log.Printf("error occurred while checking the returned result from database after insertion: %v", err)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "error occurred while checking the returned result",
		})
		return
	}

	// if no record was inserted, let us say client has failed
	if n == 0 {
		e := "could not insert the record, please try again after sometime"
		log.Println(e)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "could not insert the record",
		})
		return
	}

	// NOTE:
	//
	// Here I wanted to return the location for newly created Album but this
	// 'pq' library does not support, LastInsertionID functionality.
	m := "successfully created the record"
	log.Println(m)
	ctx.JSON(http.StatusOK, gin.H{
		"message": m,
	})

}
