package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (env Env) Delete(c *gin.Context) {

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		e := fmt.Sprintf("received invalid id path param which is not string: %v", c.Param("id"))
		log.Println(e)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "received invalid id",
		})
		return
	}
	q := `DELETE FROM roles WHERE id = $1;`
	result, err := env.DB.Exec(q, id)
	if err != nil {
		e := fmt.Sprintf("error occurred while deleting artist record with id: %d and error is: %v", id, err)
		log.Println(e)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": e,
		})
		return
	}

	// checking the number of rows affected
	n, err := result.RowsAffected()
	if err != nil {
		e := fmt.Sprintf("error occurred while checking the returned result from database after deletion: %v", err)
		log.Println(e)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": e,
		})
		return
	}

	// if no record was deleted, let us inform that there might be no
	// records to delete for this given album ID.
	if n == 0 {
		e := "could not delete the record, there might be no records for the given ID"
		log.Println(e)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": e,
		})
		return
	}

	m := "successfully deleted the record"
	log.Println(m)
	c.JSON(http.StatusOK, gin.H{
		"message": m,
	})
}
