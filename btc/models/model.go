package models

type InputDetails struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}
