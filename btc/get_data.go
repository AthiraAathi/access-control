package main

import (
	"btc/models"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func (env Env) Data(ctx *gin.Context) {
	page := ctx.Query("page")
	if page == "" {
		e := "missing query param: page"
		log.Println(e)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "missing query param: page ",
		})
		return
	}

	perPage := ctx.Query("perPage")
	if perPage == "" {
		e := "missing query param: perPage"
		log.Println(e)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "missing query param: perPage ",
		})
		return
	}

	limit, err := strconv.Atoi(perPage)
	if err != nil {
		e := fmt.Sprintf("received invalid page query param which is not integer : %v", page)
		log.Println(e)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "received invalid page query param which is not integer  ",
		})
		return
	}

	if limit > 25 {
		// Seems some bad user or front end developer playing with query params!
		e := fmt.Sprintf("we agreed to fetch less than %d records but we received request for %d", 25, limit)
		log.Println(e)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "exceed limit ",
		})
		return
	}

	offset, err := strconv.Atoi(page)
	if err != nil {
		e := fmt.Sprintf("received invalid offset query param which is not integer : %v", page)
		log.Println(e)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "received invalid offset query param which is not integer ",
		})
		return
	}

	// check if offset is a negative value
	if offset < 0 {
		e := "offset query param cannot be negative"
		log.Println(e)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "offset query param cannot be negative",
		})
		return
	}
	fmt.Println(offset, limit)
	var info = make([]models.Info, 0)
	query := `SELECT
			id,role_name,description
 			FROM
			roles LIMIT $1 OFFSET $2`
	rows, err := env.DB.Query(query, limit, offset)
	switch err {
	case sql.ErrNoRows:
		defer rows.Close()
		e := "no rows records found in roles table to read"
		log.Println(e)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "no rows records found ",
		})
	case nil:
		defer rows.Close()
		var rowsReadErr bool
		for rows.Next() {
			var a models.Info
			err = rows.Scan(&a.Id, &a.Name, &a.Description)
			if err != nil {
				log.Printf("error occurred while reading the database rows: %v", err)
				rowsReadErr = true
				break
			}
			info = append(info, a)
		}

		if rowsReadErr {
			log.Println("we are not able to fetch few records")
		}

		ctx.JSON(http.StatusOK, info)
	default:
		defer rows.Close()
		// this should not happen
		e := "some internal database server error"
		log.Println(e)
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "Internal server error ",
		})
	}

}
